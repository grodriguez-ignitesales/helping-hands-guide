package com.ignitesales.moneymatch.guide

import com.ignitesales.mvc.service.LeadProcessor
import com.ignitesales.osgi.activator.GuideActivator

class Activator extends GuideActivator {
  override val guideDomain: String = "unique-key-from-client-js"
  override def leadProcessors: Seq[LeadProcessor] = Nil
}
