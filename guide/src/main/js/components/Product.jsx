import React from "react";
import { FormattedHTMLMessage, FormattedMessage, useIntl } from "react-intl";
import { getMessageValue } from "@ignitesales/neuro-application/src/i18n/messageUtil";
import MessageIfPresent from "@ignitesales/neuro-application/src/components/MessageIfPresent";
import ReactSVG from "react-svg";

const Product = ({ product, actionCompleted }) => {
  const intl = useIntl();

  const cssBEM = "product";

  const icon = getMessageValue(intl.locale, `${product.id}.icon`);

  const productActionCompleted = (actionName, value, append = false) => {
    actionCompleted(actionName, product.id, value, append);
  };
  const learnMoreClicked = () =>
    productActionCompleted("productDetailsClicked", "learnMore", true);

  const renderProductFeatures = () => {
    const count = getMessageValue(intl.locale, `${product.id}.features.count`);
    const features = Array(count)
      .fill(1)
      .map((v, index) => {
        const msgIndex = index + 1;
        const id = `${product.id}.features.${msgIndex}`;
        return (
          <MessageIfPresent id={id} key={id}>
            <FormattedHTMLMessage id={id} tagName="li" />
          </MessageIfPresent>
        );
      });
    return features.length ? (
      <div className={`${cssBEM}__features`}>
        <ul>{features}</ul>
      </div>
    ) : null;
  };

  const renderProductDisclosures = () => {
    const count = getMessageValue(
      intl.locale,
      `${product.id}.disclosures.count`
    );
    const features = Array(count)
      .fill(1)
      .map((v, index) => {
        const msgIndex = index + 1;
        const id = `${product.id}.disclosures.${msgIndex}`;
        return (
          <MessageIfPresent id={id} key={id}>
            <FormattedHTMLMessage id={id} tagName="li" />
          </MessageIfPresent>
        );
      });
    return features.length ? (
      <div className={`${cssBEM}__disclosures`}>
        <div className="title">
          <FormattedMessage id="products.disclosures" />
        </div>
        <ol>{features}</ol>
      </div>
    ) : null;
  };

  return (
    <div className={cssBEM} key={product.id}>
      <div className={`${cssBEM}__header`}>
        <h1>
          <FormattedMessage id={`${product.id}.name`} />
        </h1>
      </div>
      <div className={`${cssBEM}__icon`}>
        <ReactSVG src={icon} />
      </div>
      <div className={`${cssBEM}__didyou`}>
        <div>
          Did you know?
        </div>
        <div>
          <FormattedMessage id={`${product.id}.didyouknow`} />
        </div>
        <img src="/classpath/assets/images/icons/didyou.png" />
      </div>
      <div className={`${cssBEM}__description`}>
        <MessageIfPresent id={`${product.id}.description.abbreviated`}>
          <p>
            <FormattedHTMLMessage
              id={`${product.id}.description.abbreviated`}
            />
          </p>
        </MessageIfPresent>
        
        {renderProductFeatures()}

        <MessageIfPresent id={`${product.id}.url`}>
          <FormattedMessage id={`${product.id}.url`}>
            {(url) => (
              <span className={`${cssBEM}__url`} onClick={learnMoreClicked}>
                <a href={url} target="_blank" rel="noopener noreferrer">
                  Learn More
                </a>
              </span>
            )}
          </FormattedMessage>
        </MessageIfPresent>

        {renderProductDisclosures()}
      </div>
    </div>
  );
};

export default Product;
