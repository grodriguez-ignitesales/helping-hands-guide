import React from 'react';
import classnames from 'classnames';
import {getMessageValue} from "@ignitesales/neuro-application/src/i18n/messageUtil";
import {FormattedMessage} from "react-intl";

const PageNavigation = ({className, nextPageClicked, previousPageClicked, disableForwardNavigation = false, processingNavigation = false, onDisabledClick, hideNext = false, hidePrevious = false, messageTag=''}) => {
  const cssBEM = 'page-navigation';
  const nextMessageId = messageTag && getMessageValue(intl.locale, `${messageTag}.nav.next`) ? `${messageTag}.nav.next` : 'nav.next';
  const prevMessageId = messageTag && getMessageValue(intl.locale, `${messageTag}.nav.prev`) ? `${messageTag}.nav.prev` : 'nav.prev';
  const classes = classnames( cssBEM, {
    [`${cssBEM}--loading`]: processingNavigation
  }, className);
  const classesPrevBtn = classnames(`${cssBEM}__prev`, 'btn', {
    disabled: processingNavigation
  });
  const classesNextBtn = classnames(`${cssBEM}__next`, 'btn', {
    disabled: processingNavigation || disableForwardNavigation
  });

  const validateBackwardClick = () => {
    previousPageClicked();
  };

  const validateForwardClick = () => {
    if(disableForwardNavigation){
      onDisabledClick({
        isReverse: false,
      });
    }else{
      nextPageClicked();
    }
  };

  const next = !hideNext ?
    <button className={classesNextBtn} onClick={validateForwardClick} disabled={processingNavigation}>
      <FormattedMessage id={nextMessageId} tagName="span"/>
    </button> : null;

  const previous = !hidePrevious ?
    <button className={classesPrevBtn} onClick={validateBackwardClick} disabled={processingNavigation}>
      <FormattedMessage id={prevMessageId} tagName="span"/>
    </button> : null;

  return <div className={classes}>
    {previous}
    {next}
  </div>;
};

export default PageNavigation;
