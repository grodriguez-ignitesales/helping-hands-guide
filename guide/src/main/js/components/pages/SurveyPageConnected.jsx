import React from 'react';
import classnames from 'classnames';
import PageNavigation from '../PageNavigation';
import QuestionGroups from '@ignitesales/neuro-application/src/components/questions/QuestionGroups';
import Footer from '../../components/Footer';
import {FormattedMessage} from 'react-intl';
import ProgressBar from '../../components/ProgressBar';

const SurveyPageConnected = ({classname, currentPage, nextPageClicked, previousPageClicked, progress, validationErrors = [], processingNavigation}) => {
  const classes = classnames(
    'surveyPage',
    `surveyPage--path-${currentPage.path.key}`,
    `surveyPage--page-${currentPage.page.key}`,
    classname
  );

  const onDisabledClick = (action) => {
    if (!action.isReverse) {
      alert(JSON.stringify(validationErrors));
    }
  };

  return (
    <>
      <ProgressBar progress={progress}/>
      <div className={classes}>
        <div className="tip">
          <FormattedMessage id="survey.title" tagName="h5"/>
          <FormattedMessage id="survey.message" values={{ br: <br />  }} tagName="span"/>
        </div>
        <div className="body">
          <div className="questionGroups clear">
            <QuestionGroups currentPage={currentPage} validationErrors={validationErrors}/>
          </div>
          <PageNavigation
            nextPageClicked={nextPageClicked}
            previousPageClicked={previousPageClicked}
            disableForwardNavigation={validationErrors.length > 0}
            processingNavigation={processingNavigation}
            onDisabledClick={onDisabledClick}
          />
        </div>
        <Footer/>
      </div>
    </>
  );
};

export default SurveyPageConnected;
