import React from 'react';
import classnames from 'classnames';
import PageNavigation from '../PageNavigation';

const SurveyPageSkeleton = () => {

  const classes = classnames(
    'surveyPage',
    'surveyPage--skeleton'
  );

  return (
    <div className={classes}>
      <div className="questionGroups clear">
        <section className="questionGroup questionGroup--centered">
          <div className="question">
            <h4 className="question__text">
              <span />
              <div className="subtext"><span /></div>
            </h4>
            <div className="question__skeleton" />
          </div>
        </section>
      </div>

      <PageNavigation className="skeleton" nextPageClicked={()=>{}} previousPageClicked={()=>{}} />
    </div>
  );
};

export default SurveyPageSkeleton;
