import getLogger from '@ignitesales/logger-es';
import React from 'react';

const logger = getLogger({ component: 'ErrorBoundary' });

class ErrorBoundary extends React.Component {
  constructor(props) {
    super(props);
    this.state = { hasError: false };
  }

  static getDerivedStateFromError() {
    return { hasError: true };
  }

  componentDidCatch(error, errorInfo) {
    logger.error('ErrorBoundary caught error', { error, errorInfo });
  }

  reset = () => {
    logger.info('oh yeah, that\'s the spot!');
    window.location = '/guide';
  };

  render() {
    if (this.state.hasError) {
      return <>
        <h1>Uh Oh, something went wrong!</h1>
        <button className="btn btn-primary" onClick={ this.reset }>Return to beginning</button>
      </>;
    }

    return this.props.children;
  }
}

export default ErrorBoundary;
