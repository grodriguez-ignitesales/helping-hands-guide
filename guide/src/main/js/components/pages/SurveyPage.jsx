import React from 'react';
import {connect} from 'react-redux';
import {goToNextPage} from '@ignitesales/neuro-application/src/actions/navigation-action-creators';
import SurveyPageConnected from './SurveyPageConnected';
import SurveyPageSkeleton from './SurveyPageSkeleton';

const SurveyPage = props => {
  let Component = SurveyPageConnected;
  if (props.incomplete) {
    Component = SurveyPageSkeleton;
  }

  return <Component {...props} />;
};

export default connect((state, {location}) => {
  const {
    pages = [],
    progress: {total = {currentPage: 0, totalPages: 0}} = {},
    validationErrors = [],
    processingNavigation = false
  } = state;

  const page = pages[location.pathname];
  const answers = state.answers || {};
  const props = {
    progress: total,
    validationErrors,
    processingNavigation
  };

  if (!page) {
    props.incomplete = true;
  } else {
    props.currentPage = {
      path: page.path,
      page: page.page,
      questionGroups: page.questionGroups,
      answers: answers
    };
  }

  return props;
}, dispatch => {
  return {
    nextPageClicked: () => {
      dispatch(goToNextPage(false));
    },
    previousPageClicked: () => {
      dispatch(goToNextPage(true));
    }
  };
})(SurveyPage);
