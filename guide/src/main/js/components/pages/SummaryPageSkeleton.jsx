import React from 'react';
import classnames from 'classnames';

function SummaryPageSkeleton() {
  const pageCssBEM = 'summaryPage';
  const classes = classnames(pageCssBEM, {
    [`${pageCssBEM}--skeleton`]: true
  });

  return (
    <div className={classes}/>
  );
}

export default SummaryPageSkeleton;

