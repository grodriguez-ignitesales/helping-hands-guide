import React from 'react';
import {Route} from 'react-router-dom';
import ErrorBoundary from './components/pages/ErrorBoundary';
import PageContainer from '@ignitesales/neuro-application/src/components/PageContainer';
import {connect} from 'react-redux';
import LandingPage from './components/pages/LandingPage';
import SurveyPage from './components/pages/SurveyPage';
import SummaryPage from './components/pages/SummaryPage';
import DetailsPage from './components/pages/DetailsPage';
import Header from './components/Header';
import AppContainer
  from '@ignitesales/neuro-application/src/components/AppContainer';
import TransitionContainer
  from '@ignitesales/neuro-application/src/components/TransitionContainer';

const App = ({location}) => {
  return <AppContainer ErrorBoundary={ErrorBoundary} location={location}>
    <Header/>
    <Route render={props => (
      <PageContainer
        {...props}
        LandingPage={LandingPage}
        SurveyPage={SurveyPage}
        SummaryPage={SummaryPage}
        DetailsPage={DetailsPage}
        render={({route}) => {
          return <TransitionContainer className="pageContainer" transitionKey={location.key}>
            { route }
          </TransitionContainer>;
        }}
      />
    )}>
    </Route>
  </AppContainer>;
};

export default connect((state,{location}) => {
  const page = state.pages[location.pathname] || {};

  return {
    location,
    page
  };
})(App);
