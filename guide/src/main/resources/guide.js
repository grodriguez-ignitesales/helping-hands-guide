db.guides.update(
  { guideId: 0 },
  {
    $set: {
      guideId: NumberLong(0),
      active: true,
      clientKey: "UniqueKeyFromClientJs",
      domain: "unique-key-from-client-js",
      name: "Name Goes Here",
      guideType: "consumer",
      delivery: "online",
      theme: "base",
      pathMode: "single",

      paths: [
        {
          key: "entry",
          pages: [],
        },
        {
          key: "consumer",
          pages: [],
        },
        {
          key: "exit",
          pages: [],
        },
      ],

      calculatedQuestions: [
        {
          appkey: "isCurrentCustomer",
          type: "calculated",
          answers: [
            {
              key:
                "hasAnsweredWith('isCustomer', {'personal', 'business', 'both'}) ? true : false",
            },
          ],
          attributes: {
            hidden: true,
          },
        },
      ],

      productGroups: [
        {
          name: "default",
          count: {
            summary: 2,
            details: 2,
          },
          groups: [
            {
              name: "checking",
              count: {
                summary: 1,
                details: 1,
              },
              products: [],
            },
            {
              name: "savings",
              count: {
                summary: 1,
                details: 1,
              },
              products: [],
            },
          ],
        },
      ],
      attributes: {},
    },
  },
  { upsert: true }
);
