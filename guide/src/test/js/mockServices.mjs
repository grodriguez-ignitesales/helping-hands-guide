import { installMocks } from '@ignitesales/neuro-application';
import landingPage from './pages/mockLandingPage.mjs';
import summaryPage from './pages/mockSummaryPage.mjs';
import surveyPages from './pages/mockSurveyPages.mjs';
import addProgressToMockPages from "./utils/addProgressToMockPages";

installMocks({
  pages: [
    landingPage,
    ...addProgressToMockPages(surveyPages),
    summaryPage
  ]
});
