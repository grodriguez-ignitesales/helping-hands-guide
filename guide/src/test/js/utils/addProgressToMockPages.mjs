
const addProgressToMockPages = mockPages => {
  const totalSurveyPages = mockPages.length || 1;

  return mockPages.map((page, index) => {
    const pageIndex = index + 1;
    return {
      ...page,
      progress: {
        total: {
          currentPage: pageIndex,
          totalPages: totalSurveyPages
        }
      }
    };
  });
};

export default addProgressToMockPages;
