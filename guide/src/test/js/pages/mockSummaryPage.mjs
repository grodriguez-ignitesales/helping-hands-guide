
const mockSummaryPage = {
  type: 'summary',
  products: {
    name: "default",
    products: [],
    groups: [
      {
        name: "checking",
        products: [
          {
            id: 1000,
            name: "Personal Checking",
            groupType: "consumer-checking"
          }
        ]
      },
      {
        name: "savings",
        products: [
          {
            id: 1001,
            name: "Personal Savings",
            groupType: "consumer-savings"
          }
        ]
      }
    ]
  },
  progress: {
    total: 100
  },
  answers: []
};

export default mockSummaryPage;
