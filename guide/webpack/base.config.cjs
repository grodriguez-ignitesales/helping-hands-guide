const path = require('path');
const webpackForNeuro = require('@ignitesales/webpack-neuro-guide-module').default;

module.exports = (options) =>
  webpackForNeuro({
    options,
    modulesToTranspile: [
      "@ignitesales",
      "react-intl",
      "event-target-shim",
      "intl-pluralrules",
    ],
    distPath: path.resolve(
      __dirname,
      "../target/resources_managed_node/toserve/assets/js"
    ),
    contentPath: path.join(__dirname, "../src/main/resources/toserve"),
    port: 8080,
  });
