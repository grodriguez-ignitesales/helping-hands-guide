// Webpack config for creating the production bundle.
const config = require('./base.config.cjs');

module.exports = config({
  minify: true,
  nodeEnv: 'production'
});
